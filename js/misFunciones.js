const URL_ = "https://programacion-web---i-sem-2019.gitlab.io/persistencia/json_web/json/empresa.json";


async function leerJSON(url){
    try{
        let response = await fetch(url);
        let user = await response.json();
        return user;
    } catch(err) {
        alert(err);
    }
}

function abrirHtml(direccion){
    window.location = direccion;
}

function consultarEmpleado(){
    let codigo = document.getElementById("codigo").value;
    leerJSON(URL_).then(datos =>{
        let sucursales = datos.sucursales; //[0,1]
        let info = buscarEmp(sucursales,codigo); //[nsuc,nemp,ventas]
        let nombreS = info[0];
        let nombreEmp = info[1];
        let ventas = info[2]; // []

        document.getElementById("nombre_emp").textContent = `Nombre:  ${nombreEmp}`;
        document.getElementById("nombre_suc").textContent = `Nombre sucursal:  ${nombreS}`;
        tablaVentas(ventas);
    });

}

function consultarSucursal(){
    let numSuc = parseInt(document.getElementById("codigo_suc").value);
    let nombre = document.getElementById("nombre_suc")
    let ciudad = document.getElementById("nombre_ciudad");
    
    
    leerJSON(URL_).then(datos =>{
        let sucursales = datos.sucursales;
        let ciudades = datos.ciudades;

        consultarEstadisticas(sucursales[numSuc-1]);
        nombre.textContent = sucursales[numSuc-1].nombre_sucursal;
        ciudad.textContent = ciudades[sucursales[numSuc-1].id_ciudad].nombre_ciudad;
    });

}

function consultarEstadisticas(sucursales){

    let empleados = sucursales.empleados;
    let valor_meses = [0,0,0,0,0,0,0,0,0,0,0,0];

    console.log(valor_meses);

    for(let i = 0; i < empleados.length; i++){
        for (let j = 0; j < empleados[i].ventas.length; j++) {
            valor_meses[j]+=(empleados[i].ventas[j].valor_venta)
        }
    }

    sumaSucursales[sucursales.id_sucursal-1] = valor_meses.reduce((a,b)=>a+b);

    console.log(sumaSucursales);

    tablaEstadisticas(valor_meses);

    // for(let i = 0; i < valor_meses.length; i++){
    //     for (let j = 0; j < empleados.length; j++) {
    //         valor_meses[i]+=(empleados[j].ventas[i].valor_venta)  
    //     }
    // }

}

let tablaEstadisticas = (valor_meses =>{
    var data = new google.visualization.DataTable();
    data.addColumn("string","Mes");
    data.addColumn("number","Valor");

    data.addRows(valor_meses.length);
    let meses = ["enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre"];
    for (let i = 0; i < valor_meses.length; i++) {
        data.setCell(i,0, meses[i]);
        data.setCell(i,1, valor_meses[i]);
    }

    var table = new google.visualization.Table(document.getElementById('table_estadisticas'));

    table.draw(data, {showRowNumber: false, width: '100%', height: '100%'});
    graficarEstadisticas(valor_meses);
});

function graficarEstadisticas(valor_meses){
    var data = new google.visualization.DataTable();
    data.addColumn('number', 'Mes');
    data.addColumn('number', 'Valor');
    data.addRows(valor_meses.length);

    for (let i = 0; i < valor_meses.length; i++) {
        data.setCell(i,0, i+1);
        data.setCell(i,1, valor_meses[i]);
    }

    var options = {
      chart: {
        title: 'Gráfica de estadísticas'
      },
      width: 900,
      height: 500
    };

    var chart = new google.charts.Line(document.getElementById('grafica_estadisticas'));

    chart.draw(data, google.charts.Line.convertOptions(options));
}

let tablaVentas = (ventas =>{
    var data = new google.visualization.DataTable();
    data.addColumn("string","Mes");
    data.addColumn("number","Valor");

    data.addRows(ventas.length);
    let meses = ["enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre"];
    for (let i = 0; i < ventas.length; i++) {
        data.setCell(i,0, meses[ventas[i].id_mes-1]);
        data.setCell(i,1, ventas[i].valor_venta);
    }

    var table = new google.visualization.Table(document.getElementById('table_ventaEmp'));

    table.draw(data, {showRowNumber: false, width: '100%', height: '100%'});
    graficarVentas(ventas);
});

function graficarVentas(ventas){
    var data = new google.visualization.DataTable();
    data.addColumn('number', 'Mes');
    data.addColumn('number', 'Valor');
    data.addRows(ventas.length);

    for (let i = 0; i < ventas.length; i++) {
        data.setCell(i,0, ventas[i].id_mes);
        data.setCell(i,1, ventas[i].valor_venta);
    }

    var options = {
      chart: {
        title: 'Gráfica de ventas'
      },
      width: 900,
      height: 500
    };

    var chart = new google.charts.Line(document.getElementById('grafica_ventasEmp'));

    chart.draw(data, google.charts.Line.convertOptions(options));
}

let buscarEmp = ((sucursales, codigo) =>{
    let inf = [];
    let empleado = null;
    for (let i = 0; i < sucursales.length; i++) {
        
        empleado = sucursales[i].empleados.filter(empleado => empleado.codigo_empleado == parseInt(codigo));
        
        if(empleado.length !=0){
            inf.push(sucursales[i].nombre_sucursal)
            inf.push(empleado[0].nombre);
            inf.push(empleado[0].ventas);
            return inf;
        }
}

     


})



